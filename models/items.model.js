'user strict';
var sql = require('../database/connection');


/**
 * Item object constructor
 * @param {Object} item 
 */
var Item = function (item) {
    this.id = item.id;
    this.type = item.type;
    this.color = item.color
    this.size = item.size;
    this.stock = item.stock;
};

/**
 * Creates a item in the database
 * @param {Item} newItem 
 * @param {Callback} result 
 */
Item.createItem = async function (newItem, result) {
    await sql.query("INSERT INTO item set ?", [newItem], function (err, res) {
        if (err) {
            result(null, err);
        }
        else {
            result(null, res.insertId);
        }
    });
};
/**
 * Get a specific item in the database by id
 * @param {Number|String} itemId 
 * @param {Callback} result 
 */
Item.getItemById = function (itemId, result) {
    sql.query("Select * from item where id = ? ", itemId, function (err, res) {
        if (err) {
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};
/**
 * Get a specific item in the database by type,color and size attributes
 * @param {Item} item 
 * @param {Callback} result 
 */
Item.getItemByAttributes = async function getItemByAttributes(item, result) {
    await sql.query("Select * from item where type = ? AND color = ? AND size = ?  ", [item.type, item.color, item.size], function (err, res) {
        if (err) {
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};
/**
 * Get all items in the database
 * @param {Callback} result 
 */
Item.getAllItems = function (result) {
    sql.query("Select * from item", function (err, res) {
        if (err) {
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};
/**
 * Update a specific item in the database by id
 * @param {Number|String} id 
 * @param {Item} item 
 * @param {Callback} result 
 */
Item.updateById = function (id, item, result) {
    sql.query("UPDATE item SET  ? WHERE id = ?", [item, id], function (err, res) {
        if (err) {
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};
/**
 * Deletes a specific item in the database by id
 * @param {Number|String} id 
 * @param {Callback} result 
 */
Item.remove = function (id, result) {
    sql.query("DELETE FROM item WHERE id = ?", [id], function (err, res) {
        if (err) {
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};

module.exports = Item;