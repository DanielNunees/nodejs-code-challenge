'user strict';
var sql = require('../database/connection');

//Order object constructor
var Order = function (order) {
    this.id = order.id;
    this.itemId = order.itemId;
    this.quantity = order.quantity
};
/**
 * Creates a order in the database
 * @param {Order} newOrder 
 * @param {Callback} result 
 */
Order.createOrder = function (newOrder, result) {
    sql.query("INSERT INTO delivermoo.order (id,itemId,quantity) (SELECT null,id,? FROM delivermoo.item where stock >= ? AND id = ?)",
        [newOrder.quantity, newOrder.quantity, newOrder.itemId], function (err, res) {
            if (err) {
                result(null, err);
            }
            else {
                result(null, res.insertId);
            }
        });
};
/**
 * Gets a order in the database
 * @param {Number|String} orderId 
 * @param {Callback} result 
 */
Order.getOrderById = function (orderId, result) {
    sql.query("Select * from delivermoo.order where id = ? ", orderId, function (err, res) {
        if (err) {
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};
/**
 * Get all orders in the database
 * @param {Callback} result 
 */
Order.getAllOrders = function (result) {
    sql.query("Select * from delivermoo.order", function (err, res) {
        if (err) {
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};

module.exports = Order;