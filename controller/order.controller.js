'use strict';

var Order = require('../models/order.model');
var Item = require('../models/items.model');
var validator = require('../validators/order.validator');


/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Object}
 */
exports.list_all_orders = function (req, res) {
    Order.getAllOrders(function (err, orders) {
        if (err) {
            return res.status(500).send(err);
        }
        else {
            return res.status(200).json({ success: true, orders: orders });
        }
    });
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Object}
 */
exports.create_a_order = function (req, res) {
    //handles null error 
    let new_order = new Order(req.body);

    if (!validator.orderValidation(new_order)) { // request body is not correct
        res.status(400).send({ success: false, message: "Invalid request" });
    } else {
        Item.getItemById(new_order.itemId, function (err, item) { //check if this item exist
            if (err || item.length == 0) {
                return res.status(404).send({ success: false, message: "Item could not be found" });
            }
            else {
                Order.createOrder(new_order, function (err, orderid) { //try to place order
                    if (err)
                        return res.status(500).send({ success: false, message: err });
                    else {
                        if (orderid == 0) {
                            //Check if there is enough quantity
                            return res.status(400).json({ success: false, message: "Item does not have enough stock" });
                        }
                        else {
                            item[0].stock -= new_order.quantity; //reduce the stock quantity
                            Item.updateById(new_order.itemId, item[0], function (err, item) { }); //update the value
                            Order.getOrderById(orderid, function (err, order) {
                                return res.status(200).json({ success: true, message: order });
                            })
                        }
                    }
                });
            }
        })
    }
};


/**
 * Gets a database order by id
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Object}
 */
exports.read_a_order = function (req, res) {
    Order.getOrderById(req.params.orderId, function (err, item) {
        if (err) {
            return res.send({ success: true, item: err });
        }
        else {
            if (item.length == 0) {
                return res.json({ success: false, message: "Order could not be found" });
            }
            else return res.json({ success: true, item: item });
        }
    });
};

