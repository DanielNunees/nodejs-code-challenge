'use strict';

var Item = require('../models/items.model');
var validator = require('../validators/item.validator');
var admin = require("../validators/admin.validator");

/**
 * Get all database items
 * @param {Request} req 
 * @param {Response} res 
 */
exports.list_all_items = function (req, res) {
    Item.getAllItems(function (err, items) {
        if (err) {
            return res.send(err);
        }
        else {
            return res.json({ success: true, items: items });
        }
    });
};

/**
 * Check if the item already exists
 * @param {Item} item 
 * @param {Response} result 
 * @returns {Object|Boolean}
 */
function checkItemExistence(item, result) {
    return new Promise(resolve => {
        Item.getItemByAttributes(item, function (err, item) {
            if (err) {
                result(err);
            }
            else {
                if (item.length == 0) {
                    resolve(false);
                }
                else {
                    resolve(item);
                }
            }
        });
    });
}


async function validations(req, res) {
    return new Promise(resolve => {
        if (!admin.adminValidation(req, res)) { //Checks if is admin
            resolve(res.status(401).send({ success: false, message: "Unauthorized" }));
        }
        if (!validator.itemRequestValidation(req) || !req.body.hasOwnProperty('items')) {//Check if there is a (valid) body
            resolve(res.status(400).send({ success: false, message: "Invalid request" }));
        }
        if (!validator.itemValidation(req.body.items) || req.body.items.length == 0) { //chech if is a valid item
            resolve(res.status(403).send({ success: false, message: "One (or more) items are invalid" }));
        }
        resolve(true);
    });
}

/**
 * Creates or updates a item
 * If found any item with the same attributes, updates
 * otherwise, creates
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Object}
 */
async function create_or_update_a_item(req, res) {
    if (await validations(req, res));
    var itemIds = [];
    for (var i = 0; i < req.body.items.length; i++) {
        var data = await checkItemExistence(req.body.items[i]); //Checks if the item already exists
        if (!data[0]) {
            let response = await create_a_item(req.body.items[i], res);
            if (response.success) itemIds.push(response.message); //get created id
            else {
                res.status(500).send({ success: false, itemIds: response.message }); //create item error 
                break;
            }
        } else {
            data[0].stock += req.body.items[i].stock; //update the old stock to new value
            let response = (await update_a_item(data[0], res));
            if (response.success) itemIds.push(data[0].id); //get updated id
            else {
                res.status(500).send({ success: false, itemIds: response.message }); //update ietm error
                break;
            }
        }
        if (itemIds.length == req.body.items.length) {
            res.status(200).send({ success: true, itemIds: itemIds });
            break;
        }
    }
}
exports.create_or_update_a_item = create_or_update_a_item;

/**
 * Creates a new item with the user entries
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Object}
 */
function create_a_item(req, res) {
    return new Promise(resolve => {
        let new_item = new Item(req);

        Item.createItem(new_item, function (err, item) {
            if (err)
                resolve({ success: false, message: err });
            else {
                resolve({ success: true, message: item });
            }
        });

    })
};


/**
 * Gets a database item by id
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Object}
 */
exports.read_a_item = function (req, res) {
    Item.getItemById(req.params.itemId, function (err, item) {
        if (err) {
            return res.status(500).send(err);
        }
        else {
            if (item.length == 0) {
                return res.status(404).json({ success: false, message: "Item could not be found" });
            }
            else return res.status(200).json({ success: true, item: item });
        }
    });
};



/**
 * Updates a database item by id
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Object}
 */
async function update_a_item(req, res) {
    return new Promise(resolve => {
        let new_item = new Item(req);
        Item.updateById(req.id, new_item, function (err, item) {
            if (err)
                resolve({ success: false, message: err });
            if (item.affectedRows == 0) {
                resolve({ success: false, message: "Item could not be found" });
            }
            resolve({ success: true, message: "Updated" });
        });

    })
};

/**
 * Update a item stock by id
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Object}
 */
exports.update_item_stock = async function (req, res) {
    if (!admin.adminValidation(req, res)) { //Checks if is admin
        return res.status(401).send({ success: false, message: "Unauthorized" });
    }
    if (await !validator.itemRequestValidation(req) //Checks if is admin, and if it's body is correct
        || isNaN(req.body.stock)
        || !req.body.hasOwnProperty('stock')) {
        return res.status(400).send({ success: false, message: "Invalid request" });
    } else {
        Item.updateById(req.params.itemId, req.body, function (err, item) {
            if (err)
                return res.status(500).send({ success: false, message: err });
            if (item.affectedRows == 0) {
                return res.status(404).send({ success: false, message: "Item could not be found" });
            }
            return res.status(200).send({ success: true });
        });
    }
};

/**
 * Deletes a database item by id
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Object}
 */
exports.delete_a_item = async function (req, res) {
    if (!admin.adminValidation(req, res)) { //Checks if is admin
        return res.status(401).send({ success: false, message: "Unauthorized" });
    }
    Item.remove(req.params.itemId, function (err, item) {
        if (err)
            return res.send(err);
        if (item.affectedRows == 0) {
            return res.status(404).json({ success: false, message: "Item could not be found" });
        }
        return res.status(200).json({ success: true });
    });
};