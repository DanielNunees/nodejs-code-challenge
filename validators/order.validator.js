/**
 * Check if the request body is
 * the same as the interface
 * @param {Order} new_order
 * @returns {Boolean} 
 */
exports.orderValidation = (new_order) => {
    if (!new_order.itemId || !new_order.quantity) { // itemId and quantity must be present at the request body
        return false;
    }
    if (isNaN(new_order.quantity)) { //quantity must be a number
        return false;
    }
    return true;
}

/**
 * Check if has a body in the request
 * @param {Request} req 
 * @returns {Boolean} 
 */
exports.orderRequestValidation = (req) => {
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return false
    }
    return true;
}