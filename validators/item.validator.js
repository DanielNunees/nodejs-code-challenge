/**
 * Check if the item is valid
 * @param {Item} items 
 * @returns {Boolean}
 */
exports.itemValidation = (items) => {
    for (var i = 0; i < items.length; i++) {
        if (!items[i].hasOwnProperty('type') ||
            !items[i].hasOwnProperty('color') ||
            !items[i].hasOwnProperty('size') ||
            !items[i].hasOwnProperty('stock')) {
            return false;
        }
        if (isNaN(items[i].stock)) {
            return false;
        }
        if (items[i].size !== "S" && items[i].size !== "M" && items[i].size !== "G") {
            return false;
        }
    }
    return true;
}

/**
 * Check if the request is valid
 * @param {Request} req 
 * @returns {Boolean}
 */
exports.itemRequestValidation = (req) => {
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return false
    }
    return true;
}