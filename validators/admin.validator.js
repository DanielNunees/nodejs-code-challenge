/**
 * Check if is admin
 * @param {Request} req 
 * @param {Response} res 
 * @returns {Boolean}
 */
exports.adminValidation = (req, res) => {

    //get the token from the header if present
    const token = req.headers["x-access-token"] || req.headers["authorization"];
    //if no token found or not equal to the orinal token, return false (not auth)
    if (!token || token != "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXI" +
        "iLCJpYXQiOjE1NjI1NzI0NjQsImV4cCI6MTU5NDEwODQ2OSwiYXVkIjoid3d3LnN0dWRlbnRzLjJoYXRzLmNvbS" +
        "5hdSIsInN1YiI6InVzZXJAMmhhdHMuY29tLmF1IiwiR2l2ZW5OYW1lIjoiSm9obiIsIlN1cm5hbWUiOiJTbm93Iiwi" +
        "RW1haWwiOiJqb2huc25vd0AyaGF0cy5jb20uYXUiLCJSb2xlIjoiSmFuaXRvciJ9.BEEqb2ihfP0ec8TBu91T9lk0kcBK" +
        "pz1NkJv4PpyjxdE") {
        return (false);
    }
    else {
        return (true);
    }
}