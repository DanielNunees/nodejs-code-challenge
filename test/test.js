var items = require('./items.mock');
var item_stock = require('./item_stock.mock');
var sql = require('../database/connection');
var chai = require('chai');
var should = require('chai').should();
var expect = chai.expect;

var server = "https://code-challenge-node-js.appspot.com";
// var server = require('../app');

var token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXI" +
  "iLCJpYXQiOjE1NjI1NzI0NjQsImV4cCI6MTU5NDEwODQ2OSwiYXVkIjoid3d3LnN0dWRlbnRzLjJoYXRzLmNvbS" +
  "5hdSIsInN1YiI6InVzZXJAMmhhdHMuY29tLmF1IiwiR2l2ZW5OYW1lIjoiSm9obiIsIlN1cm5hbWUiOiJTbm93Iiwi" +
  "RW1haWwiOiJqb2huc25vd0AyaGF0cy5jb20uYXUiLCJSb2xlIjoiSmFuaXRvciJ9.BEEqb2ihfP0ec8TBu91T9lk0kcBK" +
  "pz1NkJv4PpyjxdE";
var item1, item2;

//Require the dev-dependencies
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

describe('Clean Database', () => {
  it('should get a list of all items in the database', async () => {
    const response = await sql.query("SELECT * FROM delivermoo.order, delivermoo.item  ");
    (expect(response).to.be.an('array'));
  });
  it('should delete the database', async () => {
    const response = await sql.query("DELETE FROM delivermoo.item ");
    (expect(response).to.be.an('Object'));
    const response2 = await sql.query("DELETE FROM delivermoo.order ");
    (expect(response2).to.be.an('Object'));
  });
})






describe('/POST items', () => {
  it('should add new items to the system', async () => {
    const res = await chai.request(server)
      .post('/items').send(items)
      .set('authorization', token);
    res.should.have.status(200);
    expect(res.body.success).to.be.equal(true);
    expect(res.body).to.be.an("Object");
    item1 = (res.body.itemIds[0]);
    item2 = (res.body.itemIds[1]);
  });
  it('if item already existing, add to its quantity', async () => {
    const res = await chai.request(server)
      .post('/items').send(items)
      .set('authorization', token);
    res.should.have.status(200);
    expect(res.body.success).to.be.equal(true);
    expect(res.body).to.be.an("Object");
  });
});



describe('/GET items', () => {

  it('it should GET all the items', async () => {
    const response = await chai.request(server)
      .get('/items');
    response.should.have.status(200);
    expect(response.body.success).to.be.equal(true);
    expect(response.body).to.be.an("Object");
    expect(response.body.items).to.be.an("Array");

  });


  it('should return a specific item', async () => {
    const response = await chai.request(server)
      .get('/item/' + item1);
    response.should.have.status(200);
    expect(response.body.success).to.be.equal(true);
    expect(response.body).to.be.an("Object");

  });


  it('should return error response if item cant be found', async () => {
    const response = await chai.request(server)
      .get('/item/9999');
    response.should.have.status(404);
    expect(response.body.success).to.be.equal(false);
    expect(response.body).to.be.an("Object");
  });
});

describe('/PATCH item/{id}', () => {
  it('should reject if caller is not an admin', async () => {
    const response = await chai.request(server)
      .patch('/item/000').send(items)
      .set('authorization', "");
    response.should.have.status(401);
    expect(response.body.success).to.be.equal(false);
    expect(response.body).to.be.an("Object");
  });
  it('should reject if request isnt valid', async () => {
    const response = await chai.request(server)
      .patch('/item/' + item1).send({ stock: "a" })
      .set('authorization', token);
    response.should.have.status(400);
    expect(response.body.success).to.be.equal(false);
    expect(response.body).to.be.an("Object");
  });
  it('should reject if cannot find item', async () => {
    const response = await chai.request(server)
      .patch('/item/9999').send({ stock: 123 })
      .set('authorization', token);
    response.should.have.status(404);
    expect(response.body.success).to.be.equal(false);
    expect(response.body).to.be.an("Object");
  });
  it('should change the quantity if item found', async () => {
    const response = await chai.request(server)
      .patch('/item/' + item2).send(item_stock)
      .set('authorization', token);
    response.should.have.status(200);
    expect(response.body.success).to.be.equal(true);
    expect(response.body).to.be.an("Object");
  });
});

describe('/DELETE item/{id}', () => {
  it('should reject if caller is not an admin', async () => {
    const response = await chai.request(server)
      .delete('/item/' + item1)
      .set('authorization', "");
    response.should.have.status(401);
    expect(response.body.success).to.be.equal(false);
    expect(response.body).to.be.an("Object");
  });
  it('should reject if cannot find item', async () => {
    const response = await chai.request(server)
      .delete('/item/9999')
      .set('authorization', token);
    response.should.have.status(404);
    expect(response.body.success).to.be.equal(false);
    expect(response.body).to.be.an("Object");
  });
  it('should delete if item found', async () => {
    const response = await chai.request(server)
      .delete('/item/' + item1)
      .set('authorization', token);
    response.should.have.status(200);
    expect(response.body.success).to.be.equal(true);
    expect(response.body).to.be.an("Object");
  });
});




describe('/POST orders', () => {
  it('should return error if invalid request', async () => {
    const response = await chai.request(server)
      .post('/orders').send({});
    response.should.have.status(400);
    expect(response.body.success).to.be.equal(false);
    expect(response.body).to.be.an("Object");
  });
  it('should return error if itemId not found', async () => {
    const response = await chai.request(server)
      .post('/orders').send({ itemId: 999, quantity: 1 })
    response.should.have.status(404);
    expect(response.body.success).to.be.equal(false);
    expect(response.body).to.be.an("Object");
  });
  it('should return error if itemId does not have enought stock', async () => {
    const response = await chai.request(server)
      .post('/orders').send({ itemId: item2, quantity: 9999 });
    response.should.have.status(400);
    expect(response.body.success).to.be.equal(false);
    expect(response.body).to.be.an("Object");

  });
  it('should return success if itemId does have enought stock', async () => {
    const response = await chai.request(server)
      .post('/orders').send({ itemId: item2, quantity: 1 })
      .set('authorization', token);
    response.should.have.status(200);
    expect(response.body.success).to.be.equal(true);
    expect(response.body).to.be.an("Object");
  });
});