var express = require('express');
var router = express.Router();
var itemController = require('../controller/item.controller');


/* Public Routes */
router
    .get('/items', itemController.list_all_items)
    .get('/item/:itemId', itemController.read_a_item);

/* Admin Routes */
router
    .post('/items', itemController.create_or_update_a_item)
    .patch('/item/:itemId', itemController.update_item_stock)
    .delete('/item/:itemId', itemController.delete_a_item);

module.exports = router;
