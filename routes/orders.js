var express = require('express');
var router = express.Router();
var orderController = require('../controller/order.controller');


/* Public Routes */

router
    .get('/orders', orderController.list_all_orders)
    .post('/orders', orderController.create_a_order)
    .get('/order/:orderId', orderController.read_a_order);

module.exports = router;
