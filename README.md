## Environment Dependencies
 - Node.js
 - NPM

## Development instructions mode

- Download the repository
- `cd /project-folder`
- `npm install`
- `npm run`

## Deploy
- `npm build`

## Test Dependencies
 - Node.js
 - NPM

 ## Test instructions
 - Download the repository
 - `cd project-folder`
 - `npm install` if you haven't ran yet
 
 - `npm test`

## Production mode instructions

- Download the repository
- `cd /project-folder`
- `npm install` if you haven't ran yet
- `npm run` 

# Examples
## `PATCH item/{id}`

`{
	“stock”: 100
}`

## `POST /items`
`{
    "items": [
        {
            "type": "Shirt1",
            "color": "Blue",
            "size": "G",
            "stock": 1222
        },
        {
            "type": "Scarf1",
            "color": "Red",
            "size": "S",
            "stock": 132
        }
    ]
}`


## `POST: /order`
`{
	“itemId”:161,
	“quantity”:10
}`